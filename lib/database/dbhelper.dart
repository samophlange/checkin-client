import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;
import 'package:sqflite/sqlite_api.dart';

class DBHelper {
  static Future<Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(dbPath, 'checkin.db'),
        onCreate: (db, version) {
      //todo: create a custom user activity table relationship
      return db.execute('CREATE TABLE user_entries('
          'id INTEGER PRIMARY KEY, title TEXT, datecreated TEXT, feelinglevel INTEGER, ,notes String)');
    }, version: 1);
  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    final db = await DBHelper.database();
    db.insert(
      table,
      data,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db = await DBHelper.database();
    var res = await db.rawQuery("SELECT * FROM $table");
    return res.toList();
  }

  static Future<void> delete(int id) async {
    final db = await DBHelper.database();
    await db.rawDelete("DELETE FROM user_entries WHERE id = ?", [id]);
  }

  // Update some record
  // int count = await database.rawUpdate(
  // 'UPDATE Test SET name = ?, value = ? WHERE name = ?',
  // ['updated name', '9876', 'some name']);

}
