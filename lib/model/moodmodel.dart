import 'package:checkin_client/util/enum.dart';

/// todo(marcusb): drill down on the MoodModels to a matrix of {High, Medium and Low}
/// on one axis and {Happy, Sad, Scared, Angry and Confused} on the other axis
/// see https://i.pinimg.com/originals/c5/e8/ce/c5e8ce5ef80d16de326fea3b97b9ea3a.jpg
class MoodModel<String> extends Enum<String> {
  const MoodModel(String val) : super(val);
  static const MoodModel HAPPY = const MoodModel("Happy");
  static const MoodModel ENERGETIC = const MoodModel("Energetic");
  static const MoodModel CONTENT = const MoodModel("Content");
  static const MoodModel SAD = const MoodModel("Sad");
  static const MoodModel LOW = const MoodModel("Low");
  static const MoodModel ANXIOUS = const MoodModel("Anxious");
  static const MoodModel DEPRESSED = const MoodModel("Depressed");
  static const MoodModel LOST = const MoodModel("Lost");
}
