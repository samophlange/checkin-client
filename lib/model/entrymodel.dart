import 'activitymodel.dart';
import 'moodmodel.dart';

/// Model to hold the data for an individual data entry
class EntryModel {
  int _id;
  String _title;
  String _dateCreated;
  int _feelingLevel;
  List<MoodModel> _moodsList = List();
  List<ActivityModel> _activityList = List();
  String _notes;

  String get title => _title;

  int get entryId => _id;

  String get entryCreated => _dateCreated;

  int get feelingLevel => _feelingLevel;

  List<MoodModel> get moods => _moodsList;

  List<ActivityModel> get activities => _activityList;

  String get notes => _notes;

  EntryModel(this._id, this._title, this._dateCreated, this._feelingLevel,
      this._moodsList, this._activityList, this._notes);

  EntryModel.create(int id, String title, int feelingLevel,
      List<MoodModel> moodsList, List<ActivityModel> activityList,
      [String notes = ""]) {
    _id = id;
    _title = title;
    _dateCreated = DateTime.now().toString();
    _feelingLevel = feelingLevel;
    _moodsList.addAll(moodsList);
    _activityList.addAll(activityList);
    _notes = notes;
  }

  // Helper to store in database
  Map<String, String> listToMap(Iterable<dynamic> list) =>
      Map.fromIterable(list);

  // EntryModel.fromJson(dynamic json) {
  //   //TODO(Not Implemented)
  // }
  //
  // Map<String, dynamic> toJson() {
  //   //TODO(Not Implemented)
  //   return null;
  // }
  //
  // Map<String, dynamic> toMap() => {
  //       //TODO(Not Implemented)
  //     };
}
