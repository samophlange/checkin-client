import 'package:checkin_client/util/enum.dart';

class ActivityModel<String> extends Enum<String> {
  const ActivityModel(String val) : super(val);
  static const ActivityModel HAPPY = const ActivityModel("Happy");
  static const ActivityModel ENERGETIC = const ActivityModel("Energetic");
  static const ActivityModel CONTENT = const ActivityModel("Content");
  static const ActivityModel SAD = const ActivityModel("Sad");
  static const ActivityModel LOW = const ActivityModel("Low");
  static const ActivityModel ANXIOUS = const ActivityModel("Anxious");
  static const ActivityModel DEPRESSED = const ActivityModel("Depressed");
  static const ActivityModel LOST = const ActivityModel("Lost");
}
